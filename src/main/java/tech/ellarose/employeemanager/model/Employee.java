package tech.ellarose.employeemanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "employee")
public class Employee implements Serializable {
    @Id
    private String id;
    private String name;
    private String email;
    private String jobTitle;
    private  String phone;
    private String imageUrl;
    private String employeeCode;

}
