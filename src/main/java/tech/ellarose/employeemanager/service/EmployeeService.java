package tech.ellarose.employeemanager.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.model.GridFSFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tech.ellarose.employeemanager.exception.UserNotFoundException;
import tech.ellarose.employeemanager.model.Employee;
import tech.ellarose.employeemanager.model.LoadFileVO;
import tech.ellarose.employeemanager.repository.EmployeeRepository;
import org.apache.commons.io.IOUtils;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class EmployeeService {
    private EmployeeRepository employeeRepository;
    private GridFsOperations operations;
    private GridFsTemplate template;
    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, GridFsOperations operations, GridFsTemplate template) {
        this.employeeRepository = employeeRepository;
        this.operations = operations;
        this.template = template;
    }
    public Employee addEmployee(Employee employee)
    {
        return employeeRepository.save(employee);
    }
    public List<Employee> findAllEmployee()
    {
        return employeeRepository.findAll();
    }
    public Employee findEmployeeById(String id)
    {
        return employeeRepository.findById(id).orElseThrow(()->new UserNotFoundException("User by Id"+id+" Not found"));
    }
    public void deleteEmployee(String id)
    {
        employeeRepository.deleteById(id);
    }

    public Employee updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public String addFile(String id, MultipartFile file) {
        DBObject metadata = new BasicDBObject();
        metadata.put("entity","Host");
        metadata.put("fileSize",file.getSize());
        metadata.put("_contentType", file.getContentType());
        metadata.put("id",id);
        metadata.put("createDate", LocalDateTime.now());
        try {
            Employee employee=  employeeRepository.findById(id).orElseThrow(()->new UserNotFoundException("User by Id"+id+" Not found"));
            String imageId= template.store(file.getInputStream(), file.getOriginalFilename(), file.getContentType(), metadata).toString();
            employee.setImageUrl("http://localhost:8080/employee/avatar/"+imageId);
            employeeRepository.save(employee);
            return imageId;
        }
        catch (IOException ex)
        {
            throw new RuntimeException();
        }
    }

    public LoadFileVO getHostFile(String id ) throws IOException {
        GridFSFile gridFSFile = template.findOne( new Query(Criteria.where("_id").is(id)) );
        LoadFileVO loadFile=new LoadFileVO();
        if (gridFSFile != null && gridFSFile.getMetadata() != null) {
            loadFile.setFilename( gridFSFile.getFilename() );
            loadFile.setFileType( gridFSFile.getMetadata().get("_contentType").toString() );
            loadFile.setFileSize( gridFSFile.getMetadata().get("fileSize").toString() );
            loadFile.setFile( IOUtils.toByteArray(operations.getResource(gridFSFile).getInputStream()) );
        }
        return loadFile;
    }
}
