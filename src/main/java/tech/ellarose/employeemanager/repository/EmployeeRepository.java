package tech.ellarose.employeemanager.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import tech.ellarose.employeemanager.model.Employee;
@Repository
public interface EmployeeRepository extends MongoRepository<Employee,String> {
}
