package tech.ellarose.employeemanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tech.ellarose.employeemanager.model.Employee;
import tech.ellarose.employeemanager.model.LoadFileVO;
import tech.ellarose.employeemanager.service.EmployeeService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping()
    public ResponseEntity<List<Employee>> getListEmployee()
    {
        return  new ResponseEntity<>(employeeService.findAllEmployee(), HttpStatus.OK);
    }
    @GetMapping("/find/{id}")
    public  ResponseEntity<Employee> findEmployee(@PathVariable("id") String  id)
    {
        return new ResponseEntity<>(employeeService.findEmployeeById(id),HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee){
        return new ResponseEntity<>(employeeService.addEmployee(employee),HttpStatus.CREATED);
    }
    @PutMapping("/update")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee){
        return new ResponseEntity<>(employeeService.updateEmployee(employee),HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{id}")
    public  ResponseEntity<?> deleteEmployee(@PathVariable("id") String  id)
    {
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/avatar")
    public ResponseEntity addAvatarFile(@RequestParam String id,@RequestParam("file") MultipartFile file ) {

        return new ResponseEntity(employeeService.addFile(id,file), HttpStatus.CREATED);
    }
    @GetMapping("avatar/{id}")
    public ResponseEntity<ByteArrayResource> getHostFile(@PathVariable String id) throws IOException {

        LoadFileVO loadFile = employeeService.getHostFile(id);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(loadFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + loadFile.getFilename() + "\"")
                .body(new ByteArrayResource(loadFile.getFile()));

    }
}
